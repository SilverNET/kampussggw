﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Devices;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using AppStudio.Services;
using AppStudio.ViewModels;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace AppStudio.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MapView : Page
    {

        Geopoint campusCenter = new Geopoint(new BasicGeoposition() { Latitude = 52.1612897, Longitude = 21.0457662 });//  {Lattitude = 52.1612897, 21.0457662});
    
        //Geopoint campusTop = new BasicGeoposition(52.165592357898994, 21.045090274078966);
        //Geopoint campusBottom = new BasicGeoposition(52.156621729335036, 21.044454245905115);
        //Geopoint campusLeft = new BasicGeoposition(52.16342061812111, 21.037212281565047);
        //Geopoint campusRight = new BasicGeoposition(52.159992078297265, 21.052812261726793);

        double campusOverviewZoomLevel = 15.5;

        public MapView()
        {
            this.InitializeComponent();
            //mapControl.Center = campusCenter;
            //mapControl.ZoomLevel = campusOverviewZoomLevel;
            //ShowMyLocationOnTheMap();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


        private void ShowBuildingList_Click(object sender, RoutedEventArgs e)
        {

        }
        private void ChangeViews_Click(object sender, RoutedEventArgs e)
        {

        }
        private void Search_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
