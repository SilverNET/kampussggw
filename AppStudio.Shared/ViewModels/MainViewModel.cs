using System;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Net.NetworkInformation;

using Windows.UI.Xaml;

using AppStudio.Services;
using AppStudio.Data;

namespace AppStudio.ViewModels
{
    public class MainViewModel : BindableBase
    {
       private SamorzadSGGWViewModel _samorzadSGGWModel;
       private ImprezyViewModel _imprezyModel;
       private PrzydatneLinkiViewModel _przydatneLinkiModel;
        private PrivacyViewModel _privacyModel;

        private ViewModelBase _selectedItem = null;

        public MainViewModel()
        {
            _selectedItem = SamorzadSGGWModel;
            _privacyModel = new PrivacyViewModel();

        }
 
        public SamorzadSGGWViewModel SamorzadSGGWModel
        {
            get { return _samorzadSGGWModel ?? (_samorzadSGGWModel = new SamorzadSGGWViewModel()); }
        }
 
        public ImprezyViewModel ImprezyModel
        {
            get { return _imprezyModel ?? (_imprezyModel = new ImprezyViewModel()); }
        }
 
        public PrzydatneLinkiViewModel PrzydatneLinkiModel
        {
            get { return _przydatneLinkiModel ?? (_przydatneLinkiModel = new PrzydatneLinkiViewModel()); }
        }

        public void SetViewType(ViewTypes viewType)
        {
            SamorzadSGGWModel.ViewType = viewType;
            ImprezyModel.ViewType = viewType;
            PrzydatneLinkiModel.ViewType = viewType;
        }

        public ViewModelBase SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                SetProperty(ref _selectedItem, value);
                UpdateAppBar();
            }
        }

        public Visibility AppBarVisibility
        {
            get
            {
                return SelectedItem == null ? AboutVisibility : SelectedItem.AppBarVisibility;
            }
        }

        public Visibility AboutVisibility
        {

         get { return Visibility.Visible; }
        }

        public void UpdateAppBar()
        {
            OnPropertyChanged("AppBarVisibility");
            OnPropertyChanged("AboutVisibility");
        }

        /// <summary>
        /// Load ViewModel items asynchronous
        /// </summary>
        public async Task LoadDataAsync(bool forceRefresh = false)
        {
            var loadTasks = new Task[]
            { 
                this.SamorzadSGGWModel.LoadItemsAsync(forceRefresh),
                this.ImprezyModel.LoadItemsAsync(forceRefresh),
                this.PrzydatneLinkiModel.LoadItemsAsync(forceRefresh),
            };
            await Task.WhenAll(loadTasks);
        }

        //
        //  ViewModel command implementation
        //
        public ICommand RefreshCommand
        {
            get
            {
                return new DelegateCommand(async () =>
                {
                    await LoadDataAsync(true);
                });
            }
        }

        public ICommand AboutCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    NavigationServices.NavigateToPage("AboutThisAppPage");
                });
            }
        }

        public ICommand PrivacyCommand
        {
            get
            {
                return new DelegateCommand(() =>
                {
                    NavigationServices.NavigateTo(_privacyModel.Url);
                });
            }
        }
    }
}
