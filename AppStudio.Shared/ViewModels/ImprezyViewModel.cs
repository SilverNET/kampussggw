using System;
using System.Windows;
using System.Windows.Input;

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

using AppStudio.Services;
using AppStudio.Data;

namespace AppStudio.ViewModels
{
    public class ImprezyViewModel : ViewModelBase<FacebookSchema>
    {
        private RelayCommandEx<FacebookSchema> itemClickCommand;
        public RelayCommandEx<Slider> DecreaseSlider
        {
            get
            {
                return new RelayCommandEx<Slider>(s => s.Value--);
            }
        }

        override public Visibility GoToSourceVisibility
        {
            get { return ViewType == ViewTypes.Detail ? Visibility.Visible : Visibility.Collapsed; }
        }

        public RelayCommandEx<Slider> IncreaseSlider
        {
            get
            {
                return new RelayCommandEx<Slider>(s => s.Value++);
            }
        }

        public RelayCommandEx<FacebookSchema> ItemClickCommand
        {
            get
            {
                if (itemClickCommand == null)
                {
                    itemClickCommand = new RelayCommandEx<FacebookSchema>(
                        (item) =>
                        {

                            NavigationServices.NavigateToPage("ImprezyDetail", item);
                        });
                }

                return itemClickCommand;
            }
        }

        override public Visibility RefreshVisibility
        {
            get { return ViewType == ViewTypes.List ? Visibility.Visible : Visibility.Collapsed; }
        }

        override public void NavigateToSectionList()
        {
            NavigationServices.NavigateToPage("ImprezyList");
        }

        override protected DataSourceBase<FacebookSchema> CreateDataSource()
        {
            return new ImprezyDataSource(); // FacebookDataSource
            }
        override protected void GoToSource()
        {
            base.GoToSource("{FeedUrl}");
        }
        override protected void NavigateToSelectedItem()
        {
            NavigationServices.NavigateToPage("ImprezyDetail");
        }
    }
}
