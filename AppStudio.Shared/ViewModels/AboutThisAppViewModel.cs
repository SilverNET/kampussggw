using System;

using Windows.ApplicationModel;

namespace AppStudio.ViewModels
{
    public class AboutThisAppViewModel
    {
        public string Publisher
        {
            get
            {
                return "Silver .NET";
            }
        }

        public string AppVersion
        {
            get
            {
                return string.Format("{0}.{1}.{2}.{3}", Package.Current.Id.Version.Major, Package.Current.Id.Version.Minor, Package.Current.Id.Version.Build, Package.Current.Id.Version.Revision);
            }
        }

        public string AboutText
        {
            get
            {
                return String.Format("Interaktywna mapa Kampusu SGGW oraz dostęp do najświeższych wiadomości dostarcza Koło Naukowe Informatyków Silver .NET {0}  Zapraszamy do odwiedzenia naszej strony i zapoznania się z innymi prowadzonymi przez nas projektami. Wszelkie sugestie i propozycje prosimy zgłaszać na adres kontakt@silver.sggw.pl lub za pośrednictwem Facebook'a. {1} Polub nasz Fanpage na Facebooku, aby śledzić nasze wydarzenia - zapraszamy na warsztaty, konferencje oraz do udziału w konkursach!", Environment.NewLine, Environment.NewLine);
            }
        }
    }
}

