﻿using AppStudio.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppStudio.Common
{
    public class BudynkoGenerator
    {

        private MapBuildingObject budynek01 = new MapBuildingObject(1, 52.16439, 21.046822, new List<string>() { "Centrum Informatyczne" });
        private MapBuildingObject budynek02 = new MapBuildingObject(2, 52.164759, 21.046522, new List<string>() { "Centrum Organizacji Badań i Szkoleń", "Wydawnictwo SGGW" });
        private MapBuildingObject budynek03 = new MapBuildingObject(3, 52.165009, 21.047144, new List<string>() { "Wydział Nauk Ekonomicznych", "Międzywydziałowe Studium Turystyki i Rekreacji" });
        private MapBuildingObject budynek04 = new MapBuildingObject(4, 52.165391, 21.047831, new List<string>() { "Wydział Nauk Humanistycznych" });
        private MapBuildingObject budynek05 = new MapBuildingObject(5, 52.165206, 21.048453, new List<string>() { "Wydział Ekonomiczno-Rolniczy" });
        private MapBuildingObject budynek06 = new MapBuildingObject(6, 52.164469, 21.047895, new List<string>() { "Wydział Ekonomiczno-Rolniczy" });
        private MapBuildingObject budynek07 = new MapBuildingObject(7, 52.164535, 21.048775, new List<string>() { "Wydział Ekonomiczno-Rolniczy" });
        private MapBuildingObject budynek08 = new MapBuildingObject(8, 52.163903, 21.049483, new List<string>() { "Chór Akademicki", "Promini", "Biuro Spraw Studenckich", "AGROKADRA", "Biuro Nauki", "Biuro Współpracy z Zagranicą" });
        private MapBuildingObject budynek09 = new MapBuildingObject(9, 52.163377, 21.049097, new List<string>() { "Aula Kryształowa" });
        private MapBuildingObject budynek10 = new MapBuildingObject(10, 52.163396, 21.047895, new List<string>() { "Administracja Uczelni", "Biuro Spraw Osobowych", "Agendy Studenckie", "KURS-AR" });
        private MapBuildingObject budynek11 = new MapBuildingObject(11, 52.16389, 21.047391, new List<string>() { "Kwestura", "Kasa", "Bank" });
        private MapBuildingObject budynek12 = new MapBuildingObject(12, 52.164693, 21.050448, new List<string>() { "Rektorat", "Kanclerz" });
        private MapBuildingObject budynek13 = new MapBuildingObject(13, 52.16366, 21.05062, new List<string>() { "Pracownie sztuki Wydziału Ogrodnictwa i Architektury Krajobrazu" });
        private MapBuildingObject budynek15 = new MapBuildingObject(15, 52.163173, 21.05106, new List<string>() { "Administracja Uczelni" });
        private MapBuildingObject budynek16 = new MapBuildingObject(16, 52.161435, 21.053184, new List<string>() { "Hala Maszyn Wydziału Technologii Drewna" });
        private MapBuildingObject budynek17 = new MapBuildingObject(17, 52.160955, 21.052004, new List<string>() { "Wydział Inżynierii Produkcji" });
        private MapBuildingObject budynek18 = new MapBuildingObject(18, 52.160553, 21.052208, new List<string>() { "Wydział Inżynierii Produkcji" });
        private MapBuildingObject budynek19 = new MapBuildingObject(19, 52.160224, 21.052562, new List<string>() { "Wydział Inżynierii Produkcji" });
        private MapBuildingObject budynek20 = new MapBuildingObject(20, 52.160639, 21.053656, new List<string>() { "Hala Maszyn Wydziału Inżynierii Produkcji", "Stacja Kontroli Pojazdów" });
        private MapBuildingObject budynek21 = new MapBuildingObject(21, 52.160132, 21.051832, new List<string>() { "Wydział Inżynierii Produkcji" });
        private MapBuildingObject budynek22 = new MapBuildingObject(22, 52.160046, 21.048238, new List<string>() { "Klinika Małych Zwierząt" });
        private MapBuildingObject budynek23 = new MapBuildingObject(23, 52.158348, 21.047294, new List<string>() { "Wydział Medycyny Weterynaryjnej", "Wydział Nauk o Zwierzętach", "Centrum Analityczne" });
        private MapBuildingObject budynek24 = new MapBuildingObject(24, 52.159704, 21.046908, new List<string>() { "Wydział Medycyny Weterynaryjnej" });
        private MapBuildingObject budynek25 = new MapBuildingObject(25, 52.158803, 21.045449, new List<string>() { "Zwierzętarnia" });
        private MapBuildingObject budynek26 = new MapBuildingObject(26, 52.15821, 21.045191, new List<string>() { "Hala Sportowa" });
        private MapBuildingObject budynek27 = new MapBuildingObject(27, 52.157664, 21.045406, new List<string>() { "Basen" });
        private MapBuildingObject budynek28 = new MapBuildingObject(28, 52.157256, 21.044666, new List<string>() { "Centrum Językowo-Sportowe" });
        private MapBuildingObject budynek29 = new MapBuildingObject(29, 52.158223, 21.043689, new List<string>() { "Stacja Uzdatniania Wody" });
        private MapBuildingObject budynek30 = new MapBuildingObject(30, 52.158697, 21.044333, new List<string>() { "Administracja Uczelni" });
        private MapBuildingObject budynek31 = new MapBuildingObject(31, 52.159013, 21.044, new List<string>() { "Administracja Uczelni" });
        private MapBuildingObject budynek32 = new MapBuildingObject(32, 52.160178, 21.04459, new List<string>() { "Wydział Nauk o Żywności", "Wydział Nauk o Żywieniu Człowieka i Konsumpcji", "Międzywydziałowe Studium Towaroznawstwa" });
        private MapBuildingObject budynek33 = new MapBuildingObject(33, 52.161086, 21.046329, new List<string>() { "Wydział Budownictwa I Inżynierii Środowiska", "Międzywydziałowe Studium Ochrony Środowiska" });
        private MapBuildingObject budynek34 = new MapBuildingObject(34, 52.162087, 21.046286, new List<string>() { "Wydział Leśny", "Wydział Technologii Drewna", "Wydział Zastosowań Informatyki I Matematyki" });
        private MapBuildingObject budynek35 = new MapBuildingObject(35, 52.160389, 21.042595, new List<string>() { "Wydział Ogrodnictwa i Architektury Krajobrazu" });
        private MapBuildingObject budynek37 = new MapBuildingObject(37, 52.161916, 21.042681, new List<string>() { "Wydział Ogrodnictwa i Architektury Krajobrazu", "Wydział Rolnictwa i Biologii", "Międzywydziałowe Studium BioTechnologii", "Międzywydziałowe Studium Gospodarki Przestrzennej" });
        private MapBuildingObject budynek38 = new MapBuildingObject(38, 52.162975, 21.039151, new List<string>() { "Dom Studencki Limba" });
        private MapBuildingObject budynek39 = new MapBuildingObject(39, 52.163482, 21.038647, new List<string>() { "IKAR hotel" });
        private MapBuildingObject budynek40 = new MapBuildingObject(40, 52.164074, 21.040717, new List<string>() { "Dom studencki HILTON" });
        private MapBuildingObject budynek41 = new MapBuildingObject(41, 52.163712, 21.0412, new List<string>() { "Dom studencki GRAND" });
        private MapBuildingObject budynek42 = new MapBuildingObject(42, 52.164101, 21.041511, new List<string>() { "Dom studencki EDEN" });
        private MapBuildingObject budynek43 = new MapBuildingObject(43, 52.163726, 21.042037, new List<string>() { "Dom studencki FENIKS" });
        private MapBuildingObject budynek44 = new MapBuildingObject(44, 52.163739, 21.042831, new List<string>() { "Dom studencki ADARA" });
        private MapBuildingObject budynek45 = new MapBuildingObject(45, 52.164068, 21.04238, new List<string>() { "Dom studencki BAZYLISZEK" });
        private MapBuildingObject budynek46 = new MapBuildingObject(46, 52.164041, 21.043282, new List<string>() { "Dom studencki CEZAR" });
        private MapBuildingObject budynek47 = new MapBuildingObject(47, 52.163772, 21.043732, new List<string>() { "Dom studencki DENDRYT" });
        private MapBuildingObject budynek48 = new MapBuildingObject(48, 52.164022, 21.044569, new List<string>() { "Biblioteka Główna", "Muzeum" });
        private MapBuildingObject budynek49 = new MapBuildingObject(49, 52.159059, 21.049075, new List<string>() { "Centrum Wodne Wydziału Budownictwa i Inżynierii Środowiska" });
        private MapBuildingObject budynek50 = new MapBuildingObject(50, 52.162646, 21.052701, new List<string>() { "Ogród Bylinowy" });
        private MapBuildingObject budynek51 = new MapBuildingObject(51, 52.164704, 21.050086, new List<string>() { "Pomnik Edwarda hrabiego Raczyńskiego" });
        private MapBuildingObject budynek52 = new MapBuildingObject(52, 52.163801, 21.048505, new List<string>() { "Pomnik Juliana Ursyna Niemcewicza" });
        private MapBuildingObject budynek53 = new MapBuildingObject(53, 52.164408, 21.045541, new List<string>() { "Pomnik Władysława Grabowskiego" });
        private MapBuildingObject budynek54 = new MapBuildingObject(54, 52.162343, 21.045988, new List<string>() { "Pomnik Stanisława Wawrzyńca Staszica" });
        private MapBuildingObject budynek55 = new MapBuildingObject(55, 52.163602, 21.044865, new List<string>() { "Pomnik Dąb Jana Pawła II" });


        private List<MapBuildingObject> listaBudynkow;

        public BudynkoGenerator()
        {
            listaBudynkow = new List<MapBuildingObject>()
            {
               budynek01, budynek02 ,budynek03,budynek04,budynek05,budynek06,budynek07,budynek08,budynek09,budynek10,
               budynek11,budynek12,budynek13, budynek15,budynek16,budynek17,budynek18,budynek19,budynek20,
               budynek21,budynek22,budynek23,budynek24,budynek25,budynek26,budynek27,budynek28,budynek29,budynek30,
               budynek31,budynek32,budynek33,budynek34,budynek35,budynek37,budynek38,budynek39,budynek40,
               budynek41,budynek42,budynek43,budynek44,budynek45,budynek46,budynek47,budynek48,budynek49,budynek50,
               budynek51,budynek52,budynek53,budynek54,budynek55
            };
        }

        public IList<MapBuildingObject> Buildings
        {
            get
            {
                return this.listaBudynkow;

            }
        }
    }
}
