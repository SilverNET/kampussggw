﻿using AppStudio.Common;
using AppStudio.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using AppStudio.Services;
using System.Windows.Input;
using AppStudio;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Text;
using Windows.UI;
using Windows.UI.Xaml.Controls.Maps;
using Windows.Foundation;

namespace AppStudio.Controls
{
    public class MapUserObject : MapObjectBase
    {
        public MapUserObject(double latitude, double longitude, int id)
            : base(id,latitude, longitude )
        {
            
        }
    }
}
