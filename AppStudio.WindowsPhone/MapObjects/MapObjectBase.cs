﻿using AppStudio.Common;
using AppStudio.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using AppStudio.Services;
using System.Windows.Input;
using AppStudio;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Text;
using Windows.UI;
using Windows.Devices.Geolocation;
using Windows.Foundation;
namespace AppStudio.Common
{
    public class MapObjectBase
    {
        /// <summary>
        /// Unikalny numer obiektu na mapie, dla budynku - nr budynku - analogicznie do mapy kampusu.
        /// </summary>
        public int ID=0;

        Grid contentGrid = new Grid();

        private Geopoint objectCoords;

        /// <summary>
        /// Tagi do wyszukiwania.
        /// </summary>
        private IEnumerable<string> tags;

        public MapObjectBase(int id, double latitude, double longitude)
        {
            objectCoords = new Geopoint(new BasicGeoposition() { Latitude = latitude, Longitude = longitude });
            this.ID = id;
        }

        public MapObjectBase(int id, Geopoint geoPoint)
        {
            objectCoords = geoPoint;
            this.ID = id;
        }

        public Grid ContentGrid
        {
            get { return contentGrid; }
            set { contentGrid = value; }
        }

        public string ObjectInfo
        {
            get { return this.ToString(); }
        }

        public Geopoint ObjectPosition
        {
            get { return objectCoords; }
        }

        public IEnumerable<string> Tags
        {
            get { return tags; }
            set { tags = value; }
        }

        public override string ToString()
        {
            return String.Format("Budynek {0}", ID);
        }
    }
}
