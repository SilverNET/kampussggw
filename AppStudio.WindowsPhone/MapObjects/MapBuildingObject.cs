﻿using AppStudio.Common;
using AppStudio.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

using AppStudio.Services;
using System.Windows.Input;
using AppStudio;
using System.Collections.ObjectModel;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Text;
using Windows.UI;
using Windows.Devices.Geolocation;

namespace AppStudio.Controls
{
    public class MapBuildingObject : MapObjectBase
    {
        /// <summary>
        /// Nice hack for taps
        /// </summary>
        private bool isNavigating = false;
        public String TagsAsString
        {
            get
            {
                return string.Join(", ", this.Tags);
            }
        }

        public int ID
        {
            get
            {
                return base.ID;
            }
        }
        public MapBuildingObject(int id, double latitude, double longitude, IEnumerable<string> tags)
            : base(id,latitude, longitude)
        {
            this.Tags = tags;

            
        }

        public MapBuildingObject(int id, Geopoint geoPos, IEnumerable<string> tags)
            : base(id, geoPos)
        {
            this.Tags = tags;

            
        }

       
        
       
        

        void ContentGrid_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            if (!isNavigating)
            {
                isNavigating = true;
                NavigationServices.NavigateToPage("MapObjectDetails", this.ID);
            }
        }

    }
}
