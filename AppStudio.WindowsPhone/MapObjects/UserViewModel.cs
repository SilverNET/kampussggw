﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Sensors;

namespace AppStudio.MapObjects
{
    public class UserViewModel
    {
        Compass _sensor = Compass.GetDefault();
        private double headingAngle;

        public double HeadingAngle
        {
            get { return headingAngle; }
            set { headingAngle = value; }
        }
        

        public UserViewModel()
        {
            this.headingAngle = 45;
            _sensor.ReadingChanged += OnCompassAngleChanged;
            
        }

        void OnCompassAngleChanged(object sender, CompassReadingChangedEventArgs e)
        {
            headingAngle = _sensor.GetCurrentReading().HeadingMagneticNorth;  
        }
    }
}
