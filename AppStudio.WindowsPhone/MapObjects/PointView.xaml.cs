﻿using AppStudio.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace AppStudio.MapObjects
{
    public sealed partial class PointView : UserControl
    {

        bool isNavigating;

        public Viewbox ContentContainer
        {
            get { return this.contentContainer; }
        }
        

        public PointView()
        {
            this.InitializeComponent();
        }

        public PointView(PointViewModel pvm):this()
        {
            this.DataContext = pvm;
        }

        private void contentContainer_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            BackgroundEllipse.Width = e.NewSize.Height;
            BackgroundEllipse.Height = e.NewSize.Height;
            NumberText.FontSize = BackgroundEllipse.Height / 2;
        }

        private void UserControl_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (!isNavigating)
            {
                isNavigating = true;
                //NavigationServices.NavigateToPage("MapObjectDetails",
                //(this.DataContext as PointViewModel).MBO);
            }
        }
    }
}
