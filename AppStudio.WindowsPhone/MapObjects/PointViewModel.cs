﻿using AppStudio.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppStudio.MapObjects
{
    public class PointViewModel
    {
        private MapBuildingObject mbo;

        public MapBuildingObject MBO
        {
            get { return mbo; }
            set { mbo = value; }
        }

        public PointViewModel()
        {

        }

        public PointViewModel(MapBuildingObject mbo)
        {
            this.MBO = mbo;
        }
    }
}
