using System;

using Windows.UI.Xaml.Controls;

using AppStudio.ViewModels;
using AppStudio.Services;

namespace AppStudio.Views
{
    public sealed partial class AboutThisAppPage : Page
    {
        public AboutThisAppPage()
        {
            this.InitializeComponent();
            AboutModel = new AboutThisAppViewModel();
            this.DataContext = this;
        }

        public AboutThisAppViewModel AboutModel { get; private set; }

        private void NavigateToFB(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            NavigationServices.NavigateTo(new Uri("https://www.facebook.com/silvernetgroupsggw"));
        }
    }
}
