﻿using AppStudio.Common;
using AppStudio.Controls;
using AppStudio.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace AppStudio.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BuildingSearchView : Page
    {
        private IList<MapBuildingObject> _buildingList;
        public BuildingSearchView()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            buildingList.ItemsSource = ((e.Parameter) as BudynkoGenerator).Buildings;
            _buildingList = ((e.Parameter) as BudynkoGenerator).Buildings;
        }

        private void buildingList_ItemClick(object sender, ItemClickEventArgs e)
        {
            NavigationServices.NavigateToPage("MapView", ((e.ClickedItem) as MapBuildingObject).ObjectPosition);
        }

        /// <summary>
        /// Text in SearchBox changed trigger
        /// </summary>
        /// <param name="sender">SearchBox</param>
        /// <param name="e">Event</param>
        private void SearchBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (SearchBox.Text.Trim() != string.Empty)
            {
                buildingList.ItemsSource = GetResults(SearchBox.Text);
            }
            else {
                buildingList.ItemsSource = _buildingList;
            }
        }

        /// <summary>
        /// Getting results of user query
        /// </summary>
        /// <param name="input">User input from SearchBox </param>
        /// <returns>List of buildings</returns>
        private IList<MapBuildingObject> GetResults(string input)
        {
            #region trim query to parts
            string[] splitters = new string[]{" ",".",",","-",";"};
            List<string> queryParts = input.Split( splitters , StringSplitOptions.RemoveEmptyEntries).ToList();
            #endregion

            #region LINQ elements
            var results = _buildingList.Where(b =>
                b.Tags.Any(t => queryParts.All(qp => 
                    removePolishChars(t.ToLower()).Contains(removePolishChars(qp.ToLower())))) ||
                queryParts.Contains(b.ID.ToString()) 
            ).ToList();

            #endregion
            return results;

        }

        private string removePolishChars(string input)
        {
            Dictionary<char,char> dic = new Dictionary<char,char>(){
                {'ą','a'},
                {'ć','c'},
                {'ę','e'},
                {'ł','l'},
                {'ń','n'},
                {'ó','o'},
                {'ś','s'},
                {'ź','z'},
                {'ż','z'}
            };

            var temp = input;

            foreach (var character in input)
	        {
	        	if (dic.ContainsKey(character))
	            {
	            	 temp = temp.Replace(character,dic[character]);
	            }
	        }

            return temp;

        }
    }
}
