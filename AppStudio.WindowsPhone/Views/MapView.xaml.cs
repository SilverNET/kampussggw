﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Runtime.InteropServices.WindowsRuntime;

using Windows.Devices;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using AppStudio.Services;
using AppStudio.ViewModels;
using AppStudio.Common;
using Windows.UI.Xaml.Shapes;
using Windows.UI;
using Windows.UI.Text;
using AppStudio.MapObjects;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using AppStudio.Controls;
using Windows.Devices.Sensors;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace AppStudio.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MapView : Page
    {
        Compass _sensor = Compass.GetDefault();

        Geopoint campusCenter = new Geopoint(new BasicGeoposition() { Latitude = 52.1612897, Longitude = 21.0457662 });

        Geopoint campusTop = new Geopoint(new BasicGeoposition() { Latitude = 52.165592357898994, Longitude = 21.045090274078966 });
        Geopoint campusBottom = new Geopoint(new BasicGeoposition() { Latitude = 52.156621729335036, Longitude = 21.044454245905115 });
        Geopoint campusLeft = new Geopoint(new BasicGeoposition() { Latitude = 52.16342061812111, Longitude = 21.037212281565047 });
        Geopoint campusRight = new Geopoint(new BasicGeoposition() { Latitude = 52.159992078297265, Longitude = 21.052812261726793 });

        double campusOverviewZoomLevel = 15.5;

        private MainViewModel MVM = new MainViewModel();

        public double HeadingValue = 90;

        UserView userView = new UserView();
        public MapView()
        {
            this.InitializeComponent();
            mapControl.Center = campusCenter;
            mapControl.ZoomLevel = campusOverviewZoomLevel;
            //this.DataContext = new UserViewModel();
            //mapControl.ZoomLevelChanged += mapControl_ZoomLevelChanged;
            
            ShowMyLocationOnTheMap();
        }

        void mapControl_ZoomLevelChanged(MapControl sender, object args)
        {
            var scaleChange = mapControl.ZoomLevel / campusOverviewZoomLevel;
            foreach (var item in mapControl.Children)
            {
                if (item is PointView)
                {
                    (item as PointView).ContentContainer.Height *= scaleChange;
                    (item as PointView).ContentContainer.Width *= scaleChange;
                }
            }
            campusOverviewZoomLevel = mapControl.ZoomLevel;
        }
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            await SetUpAMap(mapControl);
            await this.MVM.LoadDataAsync();
            if (e.Parameter != null)
            {
                if (e.Parameter.GetType() == campusCenter.GetType())
                {
                    //pacz pan jak jebany rozkminił :D
                    this.CenterMapOnPosition(e.Parameter as Geopoint);
                }
            }
        }

        private void CenterMapOnPosition(Geopoint geoposition)
        {
            mapControl.Center = geoposition;
            mapControl.ZoomLevel = 18;
        }
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>

        private async Task<bool> SetUpAMap(MapControl map)
        {
            BudynkoGenerator generator = new BudynkoGenerator();
            var tileDS = new LocalMapTileDataSource("ms-appx:///Assets/Tile_{zoomlevel}_{x}_{y}.png");
            var mask = new MapTileSource(tileDS);
            map.TileSources.Add(mask);
            if (map.Children.Count != generator.Buildings.Count)
            {
                //Setting Map Global Properties
                //map.CartographicMode = MapCartographicMode.Hybrid;
                map.Style = MapStyle.Aerial;
                map.DesiredPitch = 45;
                map.LandmarksVisible = true;
                map.PedestrianFeaturesVisible = true;
                map.MapServiceToken = "R66oclOyosFO0eQPYM05eg";

                _sensor.ReadingChanged += OnHeadingChanged;

                var points = generator.Buildings.Select(x => new PointView(new MapObjects.PointViewModel(x))).ToList();

                foreach (var point in points)
                {
                    //testy wydajnosci
                    //MapIcon mapico = new MapIcon();
                    //mapico.Title = "Space Needle";
                    //mapico.Image = RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/SmallLogo.png"));

                    //MapControl.SetNormalizedAnchorPoint(point, new Point(0.5, 1.0));
                    //MapControl.SetLocation(mapico, (point.DataContext as PointViewModel).MBO.ObjectPosition);
                    ////podmieniam point na obiekt mapicon

                    MapControl.SetNormalizedAnchorPoint(point, new Point(0.5, 1.0));
                    MapControl.SetLocation(point, (point.DataContext as PointViewModel).MBO.ObjectPosition);
                    map.Children.Add(point);
                    point.Tapped += point_Tapped;
                    //obiekt.AddObjectToMapControl(map);
                }
            }
            return true;
        }
        private async void ShowMyLocationOnTheMap()
        {
            try
            {
                Geolocator myGeolocator = new Geolocator();
                Geoposition myGeoposition = await myGeolocator.GetGeopositionAsync();
                Geocoordinate myGeocoordinate = myGeoposition.Coordinate;
                var userGeo = new Geopoint(new BasicGeoposition() { Latitude = myGeocoordinate.Latitude, Longitude = myGeocoordinate.Longitude });
                MapUserObject userControl = new MapUserObject(userGeo.Position.Latitude, userGeo.Position.Longitude, 0);
                UserView userView = new UserView();
                MapControl.SetNormalizedAnchorPoint(userView, new Point(0.5, 0.5));
                MapControl.SetLocation(userView, userGeo);
                this.mapControl.Children.Add(userView);
                if (userGeo.Position.Latitude > campusTop.Position.Latitude || userGeo.Position.Latitude < campusBottom.Position.Latitude)
                {
                    throw new Exception();
                }
                if (userGeo.Position.Longitude < campusLeft.Position.Longitude || userGeo.Position.Longitude < campusBottom.Position.Longitude)
                {
                    throw new Exception();
                }
                mapControl.Center = userGeo;
                mapControl.ZoomLevel = 18;
            }
            catch (Exception ex)
            {
                //MessageDialog.Show("Ups...Nie udało nam się Ciebie znaleźć na kampusie, masz włączoną lokalizację ?", "Oj, coś nie tak", MessageBoxButton.OK);
                mapControl.Center = campusCenter;
                mapControl.ZoomLevel = campusOverviewZoomLevel;
            }
        }

        void OnHeadingChanged(object sender, CompassReadingChangedEventArgs e)
        {
            this.HeadingValue = _sensor.GetCurrentReading().HeadingMagneticNorth;
        }

        void point_Tapped(object sender, TappedRoutedEventArgs e)
        {
            var blding = ((sender as UserControl).DataContext as PointViewModel).MBO;
            this.TitleBlock.Text = blding.ObjectInfo;
            this.TagList.ItemsSource = blding.Tags;
            this.mapControl.Center = blding.ObjectPosition;
            BuildingDetails.Height = 200;
        }

        private void ShowBuildingList_Click(object sender, RoutedEventArgs e)
        {
            NavigationServices.NavigateToPage("BuildingListView", new BudynkoGenerator());
        }
        private void ChangeViews_Click(object sender, RoutedEventArgs e)
        {
            NavigationServices.NavigateToPage("MainPage", MVM); //passes preloaded MainViewModel
        }
        private void Search_Click(object sender, RoutedEventArgs e)
        {
            NavigationServices.NavigateToPage("BuildingSearchView", new BudynkoGenerator());
        }

        private void SymbolIcon_Tapped(object sender, TappedRoutedEventArgs e)
        {
            BuildingDetails.Height = 0;
        }
    }
}
